from django.urls import path
from .views import Login, Logout, SignUp

urlpatterns = [
    path("login/", Login, name="login"),
    path("logout/", Logout, name="logout"),
    path("signup/", SignUp, name="signup")
]
