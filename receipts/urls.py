from django.urls import path
from .views import RecipeList, CreateReceipt, category_list, account_list 
from .views import create_category, create_account

urlpatterns = [
    path("", RecipeList, name="home"),
    path("create/", CreateReceipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
